# Advance JS Test

------------------------------Follow the following steps to run the project.------------------------------------
1. git clone https://gitlab.com/jayvir.r/advancejstest.git
2. cd advancejsTest
3. node index
4. For the first time, you will have to choose 2 and add user with email and password.
5. After adding user, you will have to choose 1 for login.
6. After logging in, choose the respective functionality by entering the number.
7. You have to press 0, for exiting the application.
8. After pressing 0 or 10, you will be logged out.
9. Follow the steps from 3 again, to run the application again.