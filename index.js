var prompts = require('prompts');
var addUser = require('./src/add/addUser');
var viewUser = require('./src/view/viewuser');
var viewFriends = require('./src/view/viewfriends');
var viewFriend = require('./src/view/viewfriend');
var addFriend=require('./src/add/addFriend');
var addPost=require('./src/add/addpost');
var fs = require('fs');
var signIn = require('./src/service/signIn');
var deletePost=require('./src/delete/delete');
var viewPost =require('./src/view/viewpost');
console.log(
  '------------Welcome to The App------------This App Is Made By JayVir, Jinal, Pratibha-------'
);
let loggedIn = 0;
(async function user() {

  if (loggedIn == 0) {
    console.log("1. Login");
    console.log("2. Add User");
    const login = await prompts({
      type: 'number',
      name: 'value',
      message: 'Enter Your Choice ?',

    });
    switch (login.value) {
      case 1:
        //checkEdINone;
        const email = await prompts({
          type: 'text',
          name: 'value',
          message: 'Enter Your Email ?',

        });
        const password = await prompts({
          type: 'text',
          name: 'value',
          message: 'Enter Your Password ?',

        });
        const Success = signIn(email.value, password.value);
        // console.log(Success);
        if (Success) {

          userName = email.value;
          loggedIn = 1;
          fs.writeFileSync('src/globalFile', userName, err => {
            if (err) {
              console.log('Error writing file', err);
            } else {
              console.log("Login Successfull");
            }
          })
        }
        else {
          console.log("Login Failed.... Login Again");
          user();
        }

        break;
      case 2:
       await addUser();
        user();
        break;
    }
  }

  if (loggedIn === 1) {
    console.log('\nPlease select an Option from below to continue');
    console.log('\n1. View All Users');
    console.log('2. View posts made by your friends');
    console.log('3. View Friends');
    console.log('4. View Friend');
    console.log('5. View Post');
    console.log('6. Add Friend');
    console.log('7. Add Post');
    console.log('8. Delete Post');
    console.log('9. Delete Friend');
    console.log('10.Logout');
    const response = await prompts({
      type: 'number',
      name: 'value',
      message: 'Enter Your Choice ?'
    });
    switch (response.value) {
      case 1:
        viewUser();

        break;
      case 2:
        console.log("\nFunctionality Remaining")
        break;
      case 3:
        viewFriends();
        break;
      case 4:
        let friendId = await prompts({
          type: 'number',
          name: 'value',
          message: 'Enter Friend Id ?'
        });
        viewFriend(friendId.value);
        break;
      case 5:
          await viewPost();
          break;
          
        
       
       
      case 6:
        let friend = await prompts({
          type: 'number',
          name: 'value',
          message: 'Enter Friend Id ?'
        });
        await addFriend(friend.value);
        break;
      case 7:
        await addPost();
        break;
        case 8:
            let postId = await prompts({
              type: 'number',
              name: 'value',
              message: 'Enter PostId to be deleted ?'
            });
            await deletePost(postId.value);
          break;
      case 9:
        break;
      case 10:
        fs.writeFileSync('src/globalFile', '', err => {
          if (err) {
            console.log('Error writing file', err);
          } else {
            console.log("Successfully Logged Out :) ");
          }
        })
        break;
    }

    if (!(response.value == 0 || response.value==10)) {
      user();
    }
    if (response.value == 0 || response.value==10) {
      fs.writeFileSync('src/globalFile', '', err => {
        if (err) {
          console.log('Error writing file', err);
        } else {
          console.log("Successfully Logged Out :) ");
        }
      })
      console.log('Okay Bye');
    }
  }
  
 
}

)();
