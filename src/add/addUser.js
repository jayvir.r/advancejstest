const prompts = require('prompts');
const sha512 = require('js-sha512');
const fs = require('fs');   

async function getUserData() {
    console.log("add user");
    const fullName = await prompts({
        type: 'text',  
        name: 'value',
        message: 'Enter Your Full Name ?',
    });
    const email = await prompts({
        type: 'text',
        name: 'value',
        message: 'Enter Your Email ?',
    });
    const password = await prompts({
        type: 'text',
        name: 'value',
        message: 'Create Password',
    });
    fs.readFile('src/json/User.json', 'utf-8', function(err, data) {
        if (err){
            console.log('Error reading file', err);
        } 
        var arrayOfObjects = JSON.parse(data)
        let len = arrayOfObjects.length + 1;
        arrayOfObjects.push({
            id: len,
            fullName: fullName.value,
            email: email.value,
            password: sha512(password.value)
        })    
        fs.writeFileSync('src/json/User.json', JSON.stringify(arrayOfObjects), err => {
            if (err) {
                console.log('Error writing file', err);
            } else {
                //console.log(userData);
                console.log('Successfully wrote file');
            }
        })
    })
}

module.exports = getUserData;





