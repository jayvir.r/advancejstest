const fs = require('fs');
function addFriend(friendId) {
    var userId;
    const postBuffer = fs.readFileSync('src/globalFile');
    const dataJSON = postBuffer.toString();
    fs.readFile('src/json/User.json', 'utf-8', function (err, data) {
        if (err) {
            console.log('Error reading file', err);
        }
        var arrayOfObjects = JSON.parse(data)
        for (let i = 0; i < arrayOfObjects.length; i++) {
            if (dataJSON == arrayOfObjects[i].email) {
                userId = arrayOfObjects[i].id;
                //console.log(userId);
            }
        }
        fs.readFile('src/json/Friends.json', 'utf-8', function (err, data) {
            if (err) {
                console.log('Error reading file', err);
            }
            var arrayOfObjects = JSON.parse(data)
            for (let i = 0; i < arrayOfObjects.length; i++) {
                if (userId == arrayOfObjects[i].id) {
                    arrayOfObjects[i].friends.push(friendId)
                }
                else if (friendId == arrayOfObjects[i].id) {
                    arrayOfObjects[i].friends.push(userId)
                }
            }
            fs.writeFileSync('src/json/Friends.json', JSON.stringify(arrayOfObjects), err => {
                if (err) {
                    console.log('Error writing file', err);
                } else {
                    //console.log(userData);
                    console.log('Successfully wrote file');
                }
            })
        })
    });
}
module.exports = addFriend;