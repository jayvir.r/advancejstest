const prompts = require('prompts');
const fs = require('fs');   

async function addPost() {
    console.log("add Post");
    const text = await prompts({
        type: 'text',  
        name: 'value',
        message: 'Add Text : ',
    });
    fs.readFile('src/globalFile', 'utf-8', function(err, data) {
        if (err){
            console.log('Error reading file', err);
        } 
        else{
            var postBy = data;
        }
    
    fs.readFile('src/json/Posts.json', 'utf-8', function(err, data) {
        if (err){
            console.log('Error reading file', err);
        } 
        var arrayOfObjects = JSON.parse(data)
        let len = arrayOfObjects.length + 1;
        arrayOfObjects.push({
            id: len,
            text: text.value,
            postBy: postBy,
            likes: 0
        })    
        fs.writeFileSync('src/json/Posts.json', JSON.stringify(arrayOfObjects), err => {
            if (err) {
                console.log('Error writing file', err);
            } else {
                //console.log(userData);
                console.log('Successfully wrote file');
            }
        })
    })
})
}

module.exports = addPost;