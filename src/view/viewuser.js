const fs = require('fs');

const viewUser = () => {
    const postBuffer = fs.readFileSync('src/json/User.json');
    const dataJSON = postBuffer.toString();
    const data = JSON.parse(dataJSON);
    console.log("--------------------Users List------------------------------")
    for(let i=0; i<data.length; i++){
        
        console.log('name : ' + data[i].fullName);
        console.log('email : ' + data[i].email);
        console.log("---------------------------------------------------------")
    }
};
module.exports = viewUser;
