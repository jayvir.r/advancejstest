const fs = require('fs');
var prompts = require('prompts');

async function viewPost() {
  const postBuffer = fs.readFileSync('src/json/Posts.json');

  const dataJSON = postBuffer.toString();
  console.log(dataJSON);
  let likeIt = await prompts({
    type: 'text',
    name: 'value',
    message: 'Do You Want to Like any Post ?'
  });
  if (likeIt.value === "yes") {
    let postId = await prompts({
      type: 'number',
      name: 'value',
      message: 'Enter Post Id ?'
    });

    const likePost = (postId) => {
      //console.log(postId)
      const postBuffer = fs.readFileSync('src/json/Posts.json');
      const dataJSON = JSON.parse(postBuffer);
      // console.log(dataJSON.posts[0].likes);
      for (let i = 0; i < dataJSON.posts.length; i++) {
        //console.log(dataJSON.posts[i].id)
        if (dataJSON.posts[i].id === postId.value) {
          
          var likes = dataJSON.posts[i].likes;
         // console.log(likes);
          dataJSON.posts[i].likes = likes + 1;
          //console.log(dataJSON.posts[i].likes);
          fs.writeFileSync('src/json/Posts.json', JSON.stringify(dataJSON), err => {
            if (err) {
              console.log('Error writing file', err);
            } else {

              console.log('Successfully wrote file');
            }
          })
          break;
        }
      }
    }
    likePost(postId);
  }

};

module.exports = viewPost


