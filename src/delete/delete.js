const fs = require('fs');
async function deletePost(postId) {
    const postBuffer = fs.readFileSync('src/globalFile');
    const currentUser = postBuffer.toString();
    const data= fs.readFileSync('src/json/Posts.json');
        var json = JSON.parse(data);
        var posts = json.posts;
        json.posts = posts.filter((post) => {
            //console.log(currentUser);
            //console.log(post.postBy);
            if(currentUser === post.postBy)
            {
                return post.id !== postId;
            }
            else{
                return post;
            }
        });
       // console.log(json.posts);
        fs.writeFileSync('src/json/Posts.json','', err => {
            if (err) {
                console.log('Error writing file', err);
            } else {
                console.log('Successfully wrote file');
            }
        });
        fs.writeFileSync('src/json/Posts.json',JSON.stringify(json,null,2), err => {
            if (err) {
                console.log('Error writing file', err);
            } else {
                console.log('Successfully wrote file');
            }
        });
}
module.exports = deletePost;