class PostService {
  static addPost(post) {
    // function accepts a post object, saves it in a file and returns the post object
    return {
      id: 1234,
      text: 'dummy post',
      postBy: 'User name',
      likes: 0
    };
  }

  static getposts() {
    // return's all posts made by the user's friends along with the friend's name and number of likes
    return [
      {
        id: 1234,
        text: 'dummy post',
        postBy: "Friend's name",
        likes: 12
      },
      {
        id: 1234,
        text: 'dummy post',
        postBy: "Friend's name",
        likes: 15
      }
    ];
  }

  static getPost(id) {
    // function gets post details based on the ID
    return {
      id: 1234,
      text: 'dummy post',
      postBy: "User name or friend's name",
      likes: 14
    };
  }
  static deletePost(id) {
    // deletes post based on ID
  }
}
module.exports = PostService;
