/**
 * Class represents services for users.
 */

class UserService {
  /**
   * This function use to add user
   * @param {Object}  user  user required details to add user
   * @returns {Object} success or false object
   */
  static async addUser(user) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }

  /**
   * This function use to remove user
   * @param {Integer}  userId  user primary id to remove user from your users list
   * @returns {Object} success or false object
   */
  static async removeUser(userId) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }

  /**
   * This function use to view user details
   * @param {Integer}  userId  user primary id to view user details
   * @returns {Object} user details object
   */
  static async viewUser(userId) {
    return {
      status: {
        success: true,
        message: 'success'
      },
      data: {
        id: 1,
        name: 'Alice',
        email: 'alice@mailinator.com'
      }
    };
  }

  /**
   * This function use to view all users list
   * @param {Integer}  id  Logged in user primary id to get all users
   * @returns {Object} all users object
   */
  static async listUsers(id) {
    return {
      status: {
        success: true,
        message: 'success'
      },
      data: [
        {
          id: 1,
          name: 'Alice',
          email: 'alice@mailinator.com'
        },
        {
          id: 2,
          name: 'Roy',
          email: 'roy@mailinator.com'
        }
      ]
    };
  }
}

module.exports = UserService;
