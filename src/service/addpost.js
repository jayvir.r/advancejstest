const fs = require('fs');

const book = {
  title: 'Ego is my Enemy',
  author: 'John Deo'
};

// const JSONbook = JSON.stringify(book);
// fs.writeFileSync('jsondemo.json', JSONbook);

// const dataBuffer = fs.readFileSync('jsondemo.json');
// const dataJSON = dataBuffer.toString();
// const data = JSON.parse(dataJSON);
// console.log(data.title);

// const dataBuffer = fs.readFileSync('jsondemo.json');
// const dataJSON = dataBuffer.toString();
// const data = JSON.parse(dataJSON);
// console.log(data.author);
// data.author = 'Jayvir Rathi';
// data.title = 'Change the Thoughts';
// console.log(data.author);

// const authorJSON = JSON.stringify(data);
// fs.writeFileSync('jsondemo.json', authorJSON);

const dataBuffer = fs.readFileSync('jsondemo.json');
const dataJSON = dataBuffer.toString();
const data = JSON.parse(dataJSON);
console.log(data.author);
console.log(data.title);
