/**
 * Class represents services for friend.
 */

class FriendService {
  /**
   * This function use to add friend
   * @param {Object}  friend  friend required details to add friend
   * @returns {Object} success or false object
   */
  static async addFriend(friend) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }

  /**
   * This function use to remove friend
   * @param {Integer}  friendId  friend primary id to remove friend from your friends list
   * @returns {Object} success or false object
   */
  static async removeFriend(friendId) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }

  /**
   * This function use to view friend details
   * @param {Integer}  friendId  friend primary id to view friend details
   * @returns {Object} friend details object
   */
  static async viewFriend(friendId) {
    return {
      status: {
        success: true,
        message: 'success'
      },
      data: {
        id: 1,
        name: 'Alice',
        email: 'alice@mailinator.com'
      }
    };
  }

  /**
   * This function use to view all friends list
   * @param {Integer}  id  Logged in user primary id to get all friends
   * @returns {Object} all friends object
   */
  static async viewFriends(id) {
    return {
      status: {
        success: true,
        message: 'success'
      },
      data: [
        {
          id: 1,
          name: 'Alice',
          email: 'alice@mailinator.com'
        },
        {
          id: 2,
          name: 'Roy',
          email: 'roy@mailinator.com'
        }
      ]
    };
  }
}

module.exports = FriendService;
