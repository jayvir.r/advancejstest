/**
 * Class represents services for users.
 */

class AuthService {
  /**
   * This function use to login/signin user into system.
   * @param {string}  email  email required details
   * @param {string}  password  password required details
   * @returns {Object} success or false object
   */
  static async signin(email, password) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }

  /**
   * This function use to logout from system
   * @param {string}  email  email to logout user from system
   * @returns {Object} success or false object
   */
  static async logout(email) {
    return {
      status: {
        success: true,
        message: 'success'
      }
    };
  }
}

module.exports = AuthService;
